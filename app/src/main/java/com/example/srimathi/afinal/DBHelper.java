package com.example.srimathi.afinal;

/**
 * Created by Srimathi on 26/04/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;


public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_Name = "tes";
    private static final int DB_Version = 1;
    private static final String Table_name = "data";
    private static final String Col_ID = "ID";
    private static final String Col_timestamp = "Timestamp";
    private static final String Col_1 = "Ax";
    private static final String Col_2 = "Ay";
    private static final String Col_3 = "Az";
//    private final Context myContext;

    public int offset = 0;
    public static int limit = 10;
    public long count = 0;
    public float mx, my, mz; //mean
    public float mix, miy, miz; //minimum
    public float mxx, mxy, mxz; //maximum
    public float medx, medy, medz; //median
    public double  sdx, sdy, sdz; //Standard Deviation
    public double mdevx, mdevy, mdevz; //Mean Deviation
    public double smas;
    public double irx,iry,irz;

    private String feature;
    private String filename = "review2.arff";
    private String dir = Environment.getExternalStorageDirectory().getPath();
    private String filePath = dir+File.separator+filename;

    SQLiteDatabase msqlitedatabase;

    public DBHelper(Context context)
    {
        super(context,DB_Name,null,DB_Version);
//        this.myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_table = "CREATE TABLE " + Table_name + "( " + Col_ID + " INTEGER PRIMARY KEY," + Col_timestamp + " INTEGER," + Col_1 + " REAL," + Col_2 + " REAL," + Col_3 + " REAL);";
        db.execSQL(create_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + Table_name);
        onCreate(db);


    }

    public void insertData(float ax, float ay, float az){

        msqlitedatabase = this.getWritableDatabase();
        long time = System.currentTimeMillis();
        ContentValues values = new ContentValues();
        values.put(Col_timestamp,time);
        values.put(Col_1,ax);
        values.put(Col_2,ay);
        values.put(Col_3,az);

        msqlitedatabase.insert(Table_name,null,values);
        msqlitedatabase.close();

    }

    public void compute(){

        msqlitedatabase = this.getWritableDatabase();
        Cursor cursor;

        count = DatabaseUtils.queryNumEntries(msqlitedatabase,Table_name);

        String avgx = "SELECT avg("+ Col_1 +") FROM " + Table_name + " LIMIT "+ limit + " OFFSET " + offset;
        String avgy = "SELECT avg("+ Col_2 +") FROM " + Table_name + " LIMIT "+ limit + " OFFSET " + offset;
        String avgz = "SELECT avg("+ Col_3 +") FROM " + Table_name + " LIMIT "+ limit + " OFFSET " + offset;
        String minx = "SELECT min("+ Col_1 +") FROM " + Table_name + " LIMIT "+ limit + " OFFSET " + offset;
        String miny = "SELECT min("+ Col_2 +") FROM " + Table_name + " LIMIT "+ limit + " OFFSET " + offset;
        String minz = "SELECT min("+ Col_3 +") FROM " + Table_name + " LIMIT "+ limit + " OFFSET " + offset;
        String maxx = "SELECT max("+ Col_1 +") FROM " + Table_name + " LIMIT "+ limit + " OFFSET " + offset;
        String maxy = "SELECT max("+ Col_2 +") FROM " + Table_name + " LIMIT "+ limit + " OFFSET " + offset;
        String maxz = "SELECT max("+ Col_3 +") FROM " + Table_name + " LIMIT "+ limit + " OFFSET " + offset;
        String mdx = "SELECT avg(" + Col_1 +") FROM (SELECT " + Col_1 + " FROM "+ Table_name + " ORDER BY "+ Col_1+" LIMIT 2 - (SELECT COUNT(*) FROM "+Table_name+" ORDER BY "+Col_1+" ASC) %2 OFFSET (SELECT (COUNT(*)-1)/2 FROM "+Table_name+" ORDER BY "+Col_1+" ASC))";
        String mdy = "SELECT avg(" + Col_2 +") FROM (SELECT " + Col_2 + " FROM "+ Table_name + " ORDER BY "+ Col_2+" LIMIT 2 - (SELECT COUNT(*) FROM "+Table_name+" ORDER BY "+Col_2+" ASC) %2 OFFSET (SELECT (COUNT(*)-1)/2 FROM "+Table_name+" ORDER BY "+Col_2+" ASC))";
        String mdz = "SELECT avg(" + Col_3 +") FROM (SELECT " + Col_3 + " FROM "+ Table_name + " ORDER BY "+ Col_3+" LIMIT 2 - (SELECT COUNT(*) FROM "+Table_name+" ORDER BY "+Col_3+" ASC) %2 OFFSET (SELECT (COUNT(*)-1)/2 FROM "+Table_name+" ORDER BY "+Col_3+" ASC))";
        String stdx = "SELECT " + Col_1 + " FROM " + Table_name + " LIMIT " + limit + " OFFSET " + offset;
        String stdy = "SELECT " + Col_2 + " FROM " + Table_name + " LIMIT " + limit + " OFFSET " + offset;
        String stdz = "SELECT " + Col_3 + " FROM " + Table_name + " LIMIT " + limit + " OFFSET " + offset;
        String sma = "SELECT * FROM " + Table_name + " LIMIT " + limit + " OFFSET " + offset;
        String iqrx1 = "SELECT " + Col_1 + " FROM " + Table_name + " ASC LIMIT " + limit/3 + " OFFSET 1";
        String iqrx2 = "SELECT " + Col_1 + " FROM " + Table_name + " ASC LIMIT " + limit/3+5 + " OFFSET 1";
        String iqry1 = "SELECT " + Col_2 + " FROM " + Table_name + " ASC LIMIT " + limit/3 + " OFFSET 1";
        String iqry2 = "SELECT " + Col_2 + " FROM " + Table_name + " ASC LIMIT " + limit/3+5 + " OFFSET 1";
        String iqrz1 = "SELECT " + Col_3 + " FROM " + Table_name + " ASC LIMIT " + limit/3 + " OFFSET 1";
        String iqrz2 = "SELECT " + Col_3 + " FROM " + Table_name + " ASC LIMIT " + limit/3+5 + " OFFSET 1";
        String q = "DELETE FROM "+Table_name;

        double x,y,z;
        double sum = 0;
        double[] arr = new double[10];
        int i=0;

        while (offset <= count ){
            if((count-offset) > 10) {
                cursor = msqlitedatabase.rawQuery(avgx, null);
                if (cursor.moveToFirst( )) {
                    mx = cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(avgy, null);
                if (cursor.moveToFirst( )) {
                    my = cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(avgz, null);
                if (cursor.moveToFirst( )) {
                    mz = cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(minx, null);
                if (cursor.moveToFirst( )) {
                    mix = cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(miny, null);
                if (cursor.moveToFirst( )) {
                    miy = cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(minz, null);
                if (cursor.moveToFirst( )) {
                    miz = cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(maxx, null);
                if (cursor.moveToFirst( )) {
                    mxx = cursor.getFloat(0);
                }
                cursor = msqlitedatabase.rawQuery(maxy, null);
                if (cursor.moveToFirst( )) {
                    mxy = cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(maxz, null);
                if (cursor.moveToFirst( )) {
                    mxz = cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(mdx,null);
                if (cursor.moveToFirst()){
                    medx = cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(mdy,null);
                if (cursor.moveToFirst()){
                    medy = cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(mdz,null);
                if (cursor.moveToFirst()){
                    medz = cursor.getFloat(0);
                }
                cursor.close();
                i = 0;
                cursor = msqlitedatabase.rawQuery(stdx , null);
                cursor.moveToFirst();
                do{
                    x = cursor.getFloat(0) - mx;
                    arr[i++] = Math.pow(x,2);
                    cursor.moveToNext();
                }while (i<limit);
                for (i=0;i<limit;i++){
                    sum = sum+arr[i];
                }
                sdx = sum/limit;
                sum = 0;
                i = 0;
                cursor.close();
                cursor = msqlitedatabase.rawQuery(stdy , null);
                cursor.moveToFirst();
                do{
                    y = cursor.getFloat(0) - my;
                    arr[i++] = Math.pow(y,2);
                    cursor.moveToNext();
                }while (i<limit);
                for (i=0;i<limit;i++){
                    sum = sum+arr[i];
                }
                sdy = sum/limit;
                sum = 0;
                i = 0;
                cursor.close();
                cursor = msqlitedatabase.rawQuery(stdz , null);
                cursor.moveToFirst();
                do{
                    z = cursor.getFloat(0) - mz;
                    arr[i++] = Math.pow(z,2);
                    cursor.moveToNext();
                }while (i<limit);
                for (i=0;i<limit;i++){
                    sum = sum+arr[i];
                }
                sdz = sum/limit;
                sum = 0;
                i = 0;
                cursor.close();
                cursor = msqlitedatabase.rawQuery(stdx , null);
                cursor.moveToFirst();
                do{
                    if (cursor.getFloat(0)>mx){
                        arr[i++] = cursor.getFloat(0) - mx;
                    }
                    else {
                        arr[i++] = mx - cursor.getFloat(0);
                    }
                }while (i<limit);
                for (i=0;i<limit;i++){
                    sum = sum+arr[i];
                }
                mdevx = sum/limit;
                sum = 0;
                i = 0;
                cursor.close();
                cursor = msqlitedatabase.rawQuery(stdy , null);
                cursor.moveToFirst();
                do{
                    if (cursor.getFloat(0)>my){
                        arr[i++] = cursor.getFloat(0) - my;
                    }
                    else {
                        arr[i++] = my - cursor.getFloat(0);
                    }
                }while (i<limit);
                for (i=0;i<limit;i++){
                    sum = sum+arr[i];
                }
                mdevy = sum/limit;
                sum = 0;
                i = 0;
                cursor.close();
                cursor = msqlitedatabase.rawQuery(stdz , null);
                cursor.moveToFirst();
                do{
                    if (cursor.getFloat(0)>mz){
                        arr[i++] = cursor.getFloat(0) - mz;
                    }
                    else {
                        arr[i++] = mz - cursor.getFloat(0);
                    }
                }while (i<limit);
                for (i=0;i<limit;i++){
                    sum = sum+arr[i];
                }
                mdevz = sum/limit;
                sum = 0;
                cursor.close();
                cursor = msqlitedatabase.rawQuery(sma , null);
                cursor.moveToFirst();
                for (i=0;i<limit;i++){
                    sum += cursor.getFloat(2)+ cursor.getFloat(3)+ cursor.getFloat(4);
                }
                smas = sum/limit;
                cursor.close();
                cursor = msqlitedatabase.rawQuery(iqrx2 , null);
                if (cursor.moveToFirst()){
                    irx = cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(iqrx1 , null);
                if (cursor.moveToFirst()){
                    irx = irx - cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(iqry2 , null);
                if (cursor.moveToFirst()){
                    iry = cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(iqry1 , null);
                if (cursor.moveToFirst()){
                    iry = iry - cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(iqrz2 , null);
                if (cursor.moveToFirst()){
                    irz = cursor.getFloat(0);
                }
                cursor.close();
                cursor = msqlitedatabase.rawQuery(iqrz1 , null);
                if (cursor.moveToFirst()){
                    irz = irz - cursor.getFloat(0);
                }
                cursor.close();

                feature = mx+","+mdevx+","+medx+","+mix+","+mxx+","+sdx+","+irx+","+my+","+mdevy+","+medy+","+miy+","+mxy+","+sdy+","+iry+","+mz+","+mdevz+","+medz+","+miz+","+mxz+","+sdz+","+irz+",?"+"\r";
                try{
                    // Writing the extracted features in a file
                    File file = new File(filePath);
                    if(!file.exists())
                    {
                        file.createNewFile();
                        String s = "@RELATION har\r\r@ATTRIBUTE MeanX REAL\r@ATTRIBUTE MdevX REAL\r@ATTRIBUTE MedianX REAL\r@ATTRIBUTE MinX REAL\r@ATTRIBUTE MaxX REAL\r@ATTRIBUTE StdevX REAL\r@ATTRIBUTE IqrX REAL\r@ATTRIBUTE MeanY REAL\r@ATTRIBUTE MdevY REAL\r@ATTRIBUTE MedianY REAL\r@ATTRIBUTE MinY REAL\r@ATTRIBUTE MaxY REAL\r@ATTRIBUTE StdevY REAL\r@ATTRIBUTE IqrY REAL\r@ATTRIBUTE MeanZ REAL\r@ATTRIBUTE MdevZ REAL\r@ATTRIBUTE MedianZ REAL\r@ATTRIBUTE MinZ REAL\r@ATTRIBUTE MaxZ REAL\r@ATTRIBUTE StdevZ REAL\r@ATTRIBUTE IqrZ REAL\r@ATTRIBUTE class {Downstairs,Jogging,Sitting,Standing,Upstairs,Walking}\r\r@DATA\r";
                        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,true)));
                        writer.write(s);
                        writer.flush();
                        writer.close();

                    }
                    Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,true)));
                    writer.write(feature);
                    writer.flush();
                    writer.close();

                }catch(Exception e){
                    e.printStackTrace();
                }

                offset = offset + 5;
            }
            else {
                break;

            }
        }

        // Deleting the records after use
        msqlitedatabase.execSQL(q);
        offset = 0;
        msqlitedatabase.close();
    }
}


