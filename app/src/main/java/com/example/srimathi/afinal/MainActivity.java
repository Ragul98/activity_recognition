package com.example.srimathi.afinal;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Calendar;

import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;



public class MainActivity extends Activity implements SensorEventListener{
    DBHelper db;
    SQLiteDatabase sqLiteDatabase;
    private TextView x1;
    private TextView y1;
    private TextView z1;
    private TextView Result;
    private Sensor mySensor;
    private SensorManager sm;
    String dat;
    String dat1;
    String dir = Environment.getExternalStorageDirectory().getPath();
    String res = "fresult3.txt";
    String src_name = "modified.arff";
    String tst_name = "review2.arff";
    String m = "label2.arff";
    String filePath =dir+File.separator+res;//path to result
    String train = dir+File.separator+src_name;//path to trained modified.arff
    String tests = dir+File.separator+tst_name;//path to test name
    String mod_test = dir+File.separator+m;//???

    float ax,ay,az;
    float min;
    float max;
    float d;
    long times,t,t1,tot;
    int r;
    int count = 0;
    int[] numcls = new int[6];
    float act ,pass;
    StringBuffer result = new StringBuffer();
    double[] temp = new double[6];
    String[] classes = {"Downstairs","Jogging","Sitting","Standing","Upstairs","Walking"};


    @Override
    protected void onCreate(Bundle savedInstanceState){

        db = new DBHelper(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.out.println("initialised");

        times = System.currentTimeMillis();
        tot = System.currentTimeMillis();
        t1 = System.currentTimeMillis();

        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        mySensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this,mySensor,SensorManager.SENSOR_DELAY_NORMAL);//The above three lines is to get access to the accelerometer sensor


        x1 = (TextView)findViewById(R.id.xText);
        y1 = (TextView) findViewById(R.id.yText);
        z1 = (TextView) findViewById(R.id.zText);
        Result = (TextView) findViewById(R.id.textView);
    }
//sensor chenged call a function
    @Override
    public void onSensorChanged(SensorEvent event) {

        mySensor = event.sensor;

        min = mySensor.getMaximumRange() * -1;               //setting min as negation of maximum range   //-32
        max = mySensor.getMaximumRange();                   //32
        d = max - min;                   //d equals twice the max range     2*max  64
        ax = (event.values[0]-min)/d;
        ay = (event.values[1]-min)/d;
        az = (event.values[2]-min)/d;
//output screen
        x1.setText("Accelerometer X: " + event.values[0] + "");
        y1.setText("Accelerometer Y: " + event.values[1] + "");
        z1.setText("Accelerometer Z: " + event.values[2] + "");

        t = System.currentTimeMillis();
        Log.d("SENSOR",ax+" " +ay+ " "+az);
        if ((t - times) <= 60000) {//1 min
            db.insertData(ax, ay, az);//db
//            times = System.currentTimeMillis();
            Log.d("1 min",ax+" "+ay+" "+az);
        }
        else
        {
            Log.d("AFTER 1 MIN", "compute");

            db.compute(); //invoke function to compute features
            DataSource source = null;
            try {
                source = new DataSource(train);
                Log.d("compute", "adding train ok");

            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Soruce add", "failed add source failed");

            }
            try {
                Instances dataset = null;
                try {
                    dataset = source.getDataSet();
                    Log.d("load dataset","tried");
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("load dataset","failed");


                }
                Instances unlabeled = null;
                try {
                    Log.d("UNLABLEED","saving unlabeled dataset");
                    unlabeled = new Instances(new BufferedReader(new FileReader(tests)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dataset.setClassIndex(dataset.numAttributes() - 1);
                unlabeled.setClassIndex(unlabeled.numAttributes() - 1);
                Instances labeled = new Instances(unlabeled);

                J48 nb = new J48();
                try {
                    Log.d("classify","tried");
                    nb.buildClassifier(dataset);
                    Log.d("classify","done");


                } catch (Exception e) {
                    Log.d("classify","failed ");

                    e.printStackTrace();
                }


                for (int i = 0; i < unlabeled.numInstances(); i++) {
                    double clsLabel = nb.classifyInstance(unlabeled.instance(i));
                    labeled.instance(i).setClassValue(clsLabel);
                    Log.d("classify","add new to classify");
                }
                Log.d("writing to file",mod_test);


                BufferedWriter writer = new BufferedWriter(new FileWriter(mod_test));
                writer.write(labeled.toString());
                writer.newLine();
                writer.flush();
                writer.close();

                labeled.setClassIndex(labeled.numAttributes() - 1);

                Evaluation evaluation = null;
                try {
                    Log.d("Evaluation","evalating");
                    evaluation = new Evaluation(dataset);

                } catch (Exception e) {
                    Log.d("Evaluation","error");

                    e.printStackTrace();
                }
//
                Log.d("Evaluation","labeling");

                evaluation.evaluateModel(nb, labeled);
                double[][] confusion = evaluation.confusionMatrix();

                System.out.println(evaluation.toSummaryString("\nResult\n", false));
                System.out.println("Downstairs\tJogging\tSitting\tStanding\tUpstairs\tWalking");
                r = labeled.numInstances();
                Log.d("Evaluation","confusion added");

                for (int i = 0; i < labeled.numClasses(); i++) {
                    for (int j = 0; j < labeled.numClasses(); j++) {
                        System.out.print(confusion[i][j] + "\t\t\t");
                        Log.d("CONFUSION MATRIX"," "+confusion[i][j]);
                    }
                    Log.d("CONFUSION MATRIX"," "+confusion[i][i]);

                    temp[i] = confusion[i][i];
                    System.out.println();
                }

                for (int i = 0; i < labeled.numClasses(); i++) {
                    if (temp[i] > 0) {
                        Log.d("Print result"," "+classes[i]);

                        result.append(classes[i] + ",");
                        count++;
                        numcls[i]++;
                    }
                }
            } catch (Exception e) {
                Log.d("Print result","error ");

                e.printStackTrace();
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(times);
            int mYear = calendar.get(Calendar.YEAR);
            int mMonth = calendar.get(Calendar.MONTH);
            int mDay = calendar.get(Calendar.DAY_OF_MONTH);
            int mhr = calendar.get(Calendar.HOUR_OF_DAY);
            int mmin = calendar.get(Calendar.MINUTE);
            int msec = calendar.get(Calendar.SECOND);

            dat = mDay + "/" + (mMonth + 1) + "/" + mYear + "  " + mhr + ":" + mmin + ":" + msec + " " + "\t" + result + "\r";
            Log.d("TIME","added time "+dat);
            try {
                File file = new File(filePath);
                Log.d("FILE","created "+file+" "+filePath);

                if (!file.exists()) {
                    file.createNewFile();
                    Log.d("CREATE NEW FILE"," "+file);

                }
                Log.d("write"," "+dat);

                Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true)));
                writer.write(dat);
                writer.flush();
                writer.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("file","deleting ");

            result = new StringBuffer();
            new File(tests).delete();
            new File(mod_test).delete();

            times = System.currentTimeMillis();
            if (t1+180000 <= times) {
                Log.d("print time"," "+times+" " +(t1+180000));

                t1 = System.currentTimeMillis();
//                displayOutput();
//            }
//            times = System.currentTimeMillis();
//
//            if ((tot+300000) >= times){
//                tot = times;
//                new  File(filePath).delete();

                act = ((numcls[0] + numcls[1] + numcls[4] + numcls[5]) / count) * 100;
                pass = ((numcls[2] + numcls[3]) / count) * 100;
                Log.d("class"," "+act+" " +(pass));

                dat1 = "\nNow you are " + act + " % Active and " + pass + " % Idle\n";
                Log.d("class"," "+dat1);

                try {
                    File file = new File(filePath);
                    if (!file.exists()) {
                        Log.d("new file"," "+file);

                        file.createNewFile();
                    }
                    Log.d("write OS"," "+dat1);

                    Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true)));
                    writer.write(dat1);
                    writer.flush();
                    writer.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            displayOutput();

            if((tot+86400000) <= times) {
                count = 0;
                for (int i = 0; i < 6; i++) {
                    numcls[i] = 0;
                    Log.d("confusion matrix new"," "+dat1);

                }
//            new  File(filePath).delete();
            }

        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void displayOutput()
    {
        Log.d("printoutput"," s");
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                Log.d("print output"," s"+line);
                text.append('\n');
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        Result.setText(text);
    }

}

